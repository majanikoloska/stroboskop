# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
 git clone https://63160371@bitbucket.org/63160371/stroboskop.git
```


Naloga 6.2.3:
https://bitbucket.org/63160371/stroboskop/commits/4cac5cfbe2a2bc702b2cba35a03e8f55f675cc02

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/63160371/stroboskop/commits/7db59b441f79c00953e98820be30f0cfbf871c5d

Naloga 6.3.2:
https://bitbucket.org/63160371/stroboskop/commits/8de1fc031aa7330c53df8cc307984973ae647ced

Naloga 6.3.3:
https://bitbucket.org/63160371/stroboskop/commits/b2b4303af470eda10b3587e8a9ac129bbf01bb6c

Naloga 6.3.4:
https://bitbucket.org/63160371/stroboskop/commits/0c5de39e55ec6d04f72e3a6c67608cd115c5a194

Naloga 6.3.5:

```
 git checkout master
 git merge izgled
 git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/63160371/stroboskop/commits/be545a3449561b88eb5448343b30b56b3905da24

Naloga 6.4.2:
https://bitbucket.org/63160371/stroboskop/commits/f6ce4fb6403b1436ff94307a8397f1f7acead062

Naloga 6.4.3:
https://bitbucket.org/63160371/stroboskop/commits/b56c5164e35e7d9a9a7e8d77ce6c999f70c3396a

Naloga 6.4.4:
https://bitbucket.org/63160371/stroboskop/commits/04c43aba4d66ed53cb3b52f87901596bc51bef5a